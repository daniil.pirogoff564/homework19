﻿#include <iostream>

class Animal
{
protected: 
	int x;
public:
	virtual void Voice()
	{
		std::cout << "Woof!" << std::endl;
		std::cout << "Meow!" << std::endl;
		std::cout << "Kar!" << std::endl;
	}
};

class Dog : public Animal
{
public:
	void Voice() override
	{
		std::cout << "Woof!\n";
		return;
	}
};

class Cat : public Animal
{
public:
	void Voice() override
	{
		std::cout << "Meow!\n";
		return;
	}
};

class Voron : public Animal
{
public:
	void Voice() override
	{
		std::cout << "Kar!\n";
		return;
	}
};

int main()
{
	Animal* p = new Animal;
	p->Voice();
}